/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author smabe
 */
public class S5_2_Locations_TestSuite
{
    static TestMarshall instance;

    public S5_2_Locations_TestSuite()
    {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentEnvironment = Enums.Environment.coreBeta;
    }

    //FR1-Capture Locations - Main Scenario
    @Test
    public void FR1_Capture_Location_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Locations v5.2\\FR1-Capture Locations - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    //FR2-Capture State Or Province - Main Scenario
    @Test
    public void FR2_Capture_State_Or_Province_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Locations v5.2\\FR2-Capture State Or Province- Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    //FR3-Capture City Or Town - Main Scenario
    @Test
    public void FR3_Capture_City_Or_Town_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Locations v5.2\\FR3-Capture City Or Town - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    //FR4-Capture Community - Main Scenario
    @Test
    public void FR4_Capture_Community_MainScenario() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Locations v5.2\\FR4-Capture Community - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
}
